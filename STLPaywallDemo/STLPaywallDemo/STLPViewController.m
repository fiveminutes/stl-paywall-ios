//
//  STLPViewController.m
//  STLPaywallDemo
//
//  Created by Miran Brajsa on 07/02/14.
//  Copyright (c) 2014 Five Minutes Ltd. All rights reserved.
//

#import "STLPViewController.h"

#import "STLPLoginViewController.h"
#import "STLPaywallSDK.h"

#import "UIAlertView+MKBlockAdditions.h"

#define kSTLPNoAccessRights @"Access granted: no"
#define kSTLPAccessGranted @"Access granted: yes"

@interface STLPViewController () <STLPaywallSDKDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckAccessRights;
@property (weak, nonatomic) IBOutlet UILabel *lblAccessGranted;

@property (nonatomic) STLPaywallSDK *paywallSDK;
@property (nonatomic) STLPLoginViewController *loginViewController;

- (IBAction)btnCheckAccessRightsPressed:(id)sender;
- (IBAction)btnClearAccessRightsPressed:(id)sender;

@end

@implementation STLPViewController

- (void)contentAccessRequested {
    [self.paywallSDK checkAccess];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.paywallSDK = [[STLPaywallSDK alloc] initWithSyncronexDictionary:@{kSTLPSyncronexCompany:@"lee",
                                                                           kSTLPSyncronexProperty:@"stl",
                                                                           kSTLPSyncronexURLBase:@"stage.syncaccess.net"
                                                                           }
                                                          bloxDictionary:@{kSTLPBloxWSKey:@"C56E913C8AA711E3A1A40019BB2963F4",
                                                                           kSTLPBloxWSSecret:@"52EBEF24BDDFF",
                                                                           kSTLPBloxURLBase:@"dev4-dot-leetemplates-dot-com.bloxcms.com"
                                                                           }];
    self.paywallSDK.delegate = self;

    self.loginViewController = [[STLPLoginViewController alloc] initWithNibName:@"STLPLoginViewController" bundle:nil cancel:^{
        self.btnCheckAccessRights.enabled = YES;
        [UIAlertView alertViewWithTitle:@"PaywallSDK Demo" message:@"You need to log in to access premium content."];
    } completion:^(NSString *username, NSString *password) {
        [self.paywallSDK authenticateWithUsername:username password:password];
    }];

    [self.btnCheckAccessRights addObserver:self forKeyPath:@"enabled" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)dealloc {
    self.paywallSDK.delegate = nil;
    [self.btnCheckAccessRights removeObserver:self forKeyPath:@"enabled"];
}

#pragma mark KVO responder

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object != self.btnCheckAccessRights)
        return;

    if ([keyPath isEqualToString:@"enabled"]) {
        BOOL enabled = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
        if (!enabled)
            [self.activityIndicator startAnimating];
        else
            [self.activityIndicator stopAnimating];
    }
}

- (IBAction)btnCheckAccessRightsPressed:(id)sender {
    self.btnCheckAccessRights.enabled = NO;
    [self contentAccessRequested];
}

- (IBAction)btnClearAccessRightsPressed:(id)sender {
    self.lblAccessGranted.text = kSTLPNoAccessRights;
    self.loginViewController.lblDescription.text = @"Enter username and password to proceed with the login.";
    [self.paywallSDK clearPersistentData];
}

#pragma mark STLPaywallSDKDelegate

- (void)presentLoginView {
    [self presentViewController:self.loginViewController animated:YES completion:nil];
}

- (void)didRequestUsernameAndPassword:(STLPaywallSDK *)paywallSDK {
    [self presentLoginView];
}

- (void)didFinishAuthorizationWithResult:(BOOL)result {
    self.lblAccessGranted.text = (result) ? kSTLPAccessGranted : kSTLPNoAccessRights;
    self.btnCheckAccessRights.enabled = YES;

    if (!result)
        [UIAlertView alertViewWithTitle:@"PaywallSDK Demo" message:@"You need to link your account to a valid subscription."];
}

- (void)authenticationFailedWithStatusCode:(kSTLPBloxStatusCodes)bloxStatusCode message:(NSString *)message {
    //  Handle authentication errors here, e.g. non-existing users, invalid BLOX
    // key/secret values, etc.
    self.loginViewController.lblDescription.text = message;
    [self presentLoginView];
}

- (void)didFailWithAnError:(NSError *)error causedBy:(kSTLPaywallSDKErrorCauses)causedBy {
    //  Handle various errors here, e.g. timeouts, connection errors, etc.
    [UIAlertView alertViewWithTitle:@"PaywallSDK Demo" message:[NSString stringWithFormat:@"Error. %@", [error.userInfo objectForKey:@"NSLocalizedDescription"]]];
    self.btnCheckAccessRights.enabled = YES;
}

@end

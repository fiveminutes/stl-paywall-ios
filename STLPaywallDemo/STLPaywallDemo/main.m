//
//  main.m
//  STLPaywallDemo
//
//  Created by Miran Brajsa on 07/02/14.
//  Copyright (c) 2014 Five Minutes Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "STLPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([STLPAppDelegate class]));
    }
}

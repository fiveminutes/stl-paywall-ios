//
//  STLPBlox.h
//  STLPaywallSDK
//
//  Created by Miran Brajsa on 10/02/14.
//  Copyright (c) 2014 Five Minutes Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kSTLPBloxWSKey @"kSTLPBloxWSKey"
#define kSTLPBloxWSSecret @"kSTLPBloxWSSecret"
#define kSTLPBloxURLBase @"kSTLPBloxURLBase"

typedef enum {
    kSTLPBloxStatusCodeInvalidUsernameOrPassword = 0,
    kSTLPBloxStatusCodeUnauthorizedAccess = 401,
    kSTLPBloxStatusCodeSuccess = 8000
} kSTLPBloxStatusCodes;

@class STLPBlox;

@protocol STLPBloxDelegate <NSObject>
- (void)blox:(STLPBlox *)blox didAttemptAuthorizationWithStatusCode:(kSTLPBloxStatusCodes)statusCode authToken:(NSString *)authToken userId:(NSString *)userId message:(NSString *)message;
- (void)blox:(STLPBlox *)blox didFailCheckAccessWithError:(NSError *)error;
@end

@interface STLPBlox : NSObject

@property (weak, nonatomic) id<STLPBloxDelegate> delegate;
@property (nonatomic) NSDictionary *bloxDictionary;

- (void)authenticateWithUsername:(NSString *)username password:(NSString *)password;

@end

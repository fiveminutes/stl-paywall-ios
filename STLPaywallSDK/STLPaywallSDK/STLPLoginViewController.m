//
//  STLPLoginViewController.m
//  STLPaywallDemo
//
//  Created by Miran Brajsa on 13/02/14.
//  Copyright (c) 2014 Five Minutes Ltd. All rights reserved.
//

#import "STLPLoginViewController.h"

@interface STLPLoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

- (IBAction)btnCancelPressed:(id)sender;
- (IBAction)btnLoginPressed:(id)sender;

@end

@implementation STLPLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil cancel:(VoidBlock)cancel completion:(UsernamePasswordBlock)completion {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.cancel = cancel;
        self.completion = completion;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSAssert(NO, @"Should not use default initializer. Use 'initWithNibName:bundle:cancel:completion:' instead.");
    return nil;
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnCancelPressed:(id)sender {
    [self dismiss];
    if (self.cancel)
        self.cancel();
}

- (IBAction)btnLoginPressed:(id)sender {
    [self dismiss];
    if (self.completion)
        self.completion(self.txtUserName.text, self.txtPassword.text);
}

@end

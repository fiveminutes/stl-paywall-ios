//
//  STLPBlox.m
//  STLPaywallSDK
//
//  Created by Miran Brajsa on 10/02/14.
//  Copyright (c) 2014 Five Minutes Ltd. All rights reserved.
//

#import "STLPBlox.h"

#import "Base64.h"

@interface STLPBlox () <NSURLConnectionDelegate>

@property (nonatomic) NSURLConnection *connection;

@end

@implementation STLPBlox

- (void)authenticateWithUsername:(NSString *)username password:(NSString *)password {
    NSString *urlString = [NSString stringWithFormat:@"https://%@/tncms/webservice/v1/user/authenticate/", [self.bloxDictionary objectForKey:kSTLPBloxURLBase]];
    NSString *parameters = [NSString stringWithFormat:@"user=%@&password=%@", username, password];

    NSData *authenticationData = [[NSString stringWithFormat:@"%@:%@", [self.bloxDictionary objectForKey:kSTLPBloxWSKey], [self.bloxDictionary objectForKey:kSTLPBloxWSSecret]] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64EncodedAuthenticationData = [authenticationData base64EncodedString];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:15];
    [request addValue:[NSString stringWithFormat:@"Basic %@", base64EncodedAuthenticationData] forHTTPHeaderField:@"Authorization"];
    [request setHTTPBody:[parameters dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];

    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

#pragma mark NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    NSError* error;
    NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSLog(@"%@", responseData.description);
    if (error) {
        [self.delegate blox:self didFailCheckAccessWithError:error];
        NSLog(@"Filed logging in user with error - %@", error.description);
        return;
    }
    if ([responseData objectForKey:@"status"] && [[responseData objectForKey:@"status"] isEqualToString:@"error"])
        [self.delegate blox:self didAttemptAuthorizationWithStatusCode:(kSTLPBloxStatusCodes)[[responseData objectForKey:@"code"] integerValue] authToken:nil userId:nil message:[responseData objectForKey:@"message"]];
    else
        [self.delegate blox:self didAttemptAuthorizationWithStatusCode:kSTLPBloxStatusCodeSuccess authToken:[responseData objectForKey:@"authtoken"] userId:[responseData objectForKey:@"id"] message:nil];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [self.delegate blox:self didFailCheckAccessWithError:error];
    NSLog(@"Failed logging in user with error - %@", error.description);
}

@end

//
//  STLPLoginViewController.h
//  STLPaywallDemo
//
//  Created by Miran Brajsa on 13/02/14.
//  Copyright (c) 2014 Five Minutes Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^VoidBlock)();
typedef void(^UsernamePasswordBlock)(NSString *, NSString *);

@interface STLPLoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@property (copy) VoidBlock cancel;
@property (copy) VoidBlock completion;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil cancel:(VoidBlock)cancel completion:(UsernamePasswordBlock)completion;

@end

//
//  STLPSyncronex.m
//  STLPaywallSDK
//
//  Created by Miran Brajsa on 07/02/14.
//  Copyright (c) 2014 Five Minutes Ltd. All rights reserved.
//

#import "STLPSyncronex.h"

#define kSTLPSyncronexSavedUserId @"kSTLPSyncronexSavedUserId"

@interface STLPSyncronex ()

@property (nonatomic) NSURLConnection *connection;

@end

@implementation STLPSyncronex

- (id)init {
    self = [super init];
    if (self) {
        NSString *savedUserId = [[NSUserDefaults standardUserDefaults] objectForKey:kSTLPSyncronexSavedUserId];
        if (savedUserId)
            self.userId = savedUserId;
    }
    return self;
}

- (void)reportError:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self.delegate syncronex:self didFailCheckAccessWithError:error];
    });
    NSLog(@"Filed checking access with error - %@", error.description);
}

- (void)processReceivedData:(NSData *)data error:(NSError *)error {
    if (error) {
        [self reportError:error];
        return;
    }

    NSArray *responseData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if (error) {
        [self reportError:error];
        return;
    }
    NSLog(@"%@", responseData.description);

    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self.delegate syncronex:self didReceiveUserSubscriptions:responseData];
    });
}

- (void)checkAccessWorker {
    NSString *urlString = [NSString stringWithFormat:@"https://%@/%@/%@/api/svcs/subscribers/products/byexternalid/%@?format=json&source=iOS",
                           [self.syncronexDictionary objectForKey:kSTLPSyncronexURLBase],
                           [self.syncronexDictionary objectForKey:kSTLPSyncronexCompany],
                           [self.syncronexDictionary objectForKey:kSTLPSyncronexProperty],
                           self.userId];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:15];

    NSURLResponse* response;
    NSError* error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    [self processReceivedData:data error:error];
}


- (void)checkAccess {
    if (!self.userId || [self.userId isEqualToString:@""]) {
        [self.delegate syncronexDidRequestUserId:self];
    }
    else {
        NSString *savedUserId = [[NSUserDefaults standardUserDefaults] objectForKey:kSTLPSyncronexSavedUserId];
        if (!savedUserId) {
            [[NSUserDefaults standardUserDefaults] setObject:self.userId forKey:kSTLPSyncronexSavedUserId];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, (unsigned long)NULL), ^(void) {
            [self checkAccessWorker];
        });
    }
}

- (void)clearPersistentData {
    self.userId = nil;
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kSTLPSyncronexSavedUserId];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end

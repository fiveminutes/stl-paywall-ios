//
//  STLPaywallSDK.h
//  STLPaywallSDK
//
//  Created by Miran Brajsa on 07/02/14.
//  Copyright (c) 2014 Five Minutes Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "STLPBlox.h"
#import "STLPSyncronex.h"

typedef enum {
    kSTLPaywallSDKErrorCauseSyncronex,
    kSTLPaywallSDKErrorCauseBlox
} kSTLPaywallSDKErrorCauses;

@class STLPaywallSDK;

@protocol STLPaywallSDKDelegate <NSObject>
@optional
- (void)authenticationFailedWithStatusCode:(kSTLPBloxStatusCodes)bloxStatusCode message:(NSString *)message;
- (void)didFailWithAnError:(NSError *)error causedBy:(kSTLPaywallSDKErrorCauses)causedBy;
- (void)didFinishAuthorizationWithResult:(BOOL)result;
- (void)didRequestUsernameAndPassword:(STLPaywallSDK *)paywallSDK;
@end

@interface STLPaywallSDK : NSObject

@property (weak, nonatomic) id<STLPaywallSDKDelegate> delegate;

- (id)initWithSyncronexDictionary:(NSDictionary *)syncronexDictionary bloxDictionary:(NSDictionary *)bloxDictionary;

- (void)authenticateWithUsername:(NSString *)username password:(NSString *)password;
- (void)checkAccess;
- (void)clearPersistentData;

@end

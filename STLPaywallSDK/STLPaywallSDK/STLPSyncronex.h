//
//  STLPSyncronex.h
//  STLPaywallSDK
//
//  Created by Miran Brajsa on 07/02/14.
//  Copyright (c) 2014 Five Minutes Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kSTLPSyncronexCompany @"kSTLPSyncronexCompany"
#define kSTLPSyncronexProperty @"kSTLPSyncronexProperty"
#define kSTLPSyncronexURLBase @"kSTLPSyncronexURLBase"

@class STLPSyncronex;

@protocol STLPSyncronexDelegate <NSObject>
- (void)syncronex:(STLPSyncronex *)syncronex didReceiveUserSubscriptions:(NSArray *)userSubscriptions;
- (void)syncronex:(STLPSyncronex *)syncronex didFailCheckAccessWithError:(NSError *)error;
- (void)syncronexDidRequestUserId:(STLPSyncronex *)syncronex;
@end

@interface STLPSyncronex : NSObject

@property (weak, nonatomic) id<STLPSyncronexDelegate> delegate;
@property (nonatomic) NSDictionary *syncronexDictionary;
@property (nonatomic, copy) NSString *userId;

- (void)checkAccess;
- (void)clearPersistentData;

@end

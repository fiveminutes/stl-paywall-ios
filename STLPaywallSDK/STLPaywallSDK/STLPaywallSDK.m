//
//  STLPaywallSDK.m
//  STLPaywallSDK
//
//  Created by Miran Brajsa on 07/02/14.
//  Copyright (c) 2014 Five Minutes Ltd. All rights reserved.
//

#import "STLPaywallSDK.h"

#import "Base64.h"
#import <UIKit/UIKit.h>

@interface STLPaywallSDK () <STLPSyncronexDelegate, STLPBloxDelegate>

@property (strong, nonatomic) STLPSyncronex *syncronex;
@property (strong, nonatomic) STLPBlox *blox;

@end

@implementation STLPaywallSDK

- (id)init {
    NSAssert(NO, @"Parameterless initialization should not be used. Use 'initWithSyncronexDictionary:bloxDictionary:' instead.");
    return nil;
}

- (id)initWithSyncronexDictionary:(NSDictionary *)syncronexDictionary bloxDictionary:(NSDictionary *)bloxDictionary {
    self = [super init];
    if (self) {
        self.syncronex = [STLPSyncronex new];
        self.syncronex.delegate = self;
        self.syncronex.syncronexDictionary = syncronexDictionary;

        self.blox = [STLPBlox new];
        self.blox.delegate = self;
        self.blox.bloxDictionary = bloxDictionary;
    }
    return self;
}

- (void)dealloc {
    self.blox.delegate = nil;
    self.syncronex.delegate = nil;
}

- (void)authenticateWithUsername:(NSString *)username password:(NSString *)password {
    [self.blox authenticateWithUsername:username password:password];
}

- (void)checkAccess {
    [self.syncronex checkAccess];
}

- (void)clearPersistentData {
    [self.syncronex clearPersistentData];
}

#pragma mark STLPSyncronexDelegate

- (void)syncronexDidRequestUserId:(STLPSyncronex *)syncronex {
    if ([self.delegate respondsToSelector:@selector(didRequestUsernameAndPassword:)])
        [self.delegate didRequestUsernameAndPassword:self];
}

- (void)syncronex:(STLPSyncronex *)syncronex didReceiveUserSubscriptions:(NSArray *)userSubscriptions {
    if ([self.delegate respondsToSelector:@selector(didFinishAuthorizationWithResult:)])
        [self.delegate didFinishAuthorizationWithResult:(userSubscriptions.count > 0)];
}

- (void)syncronex:(STLPSyncronex *)syncronex didFailCheckAccessWithError:(NSError *)error {
    if ([self.delegate respondsToSelector:@selector(didFailWithAnError:causedBy:)])
        [self.delegate didFailWithAnError:error causedBy:kSTLPaywallSDKErrorCauseSyncronex];
}

#pragma mark STLPBloxDelegate

- (void)blox:(STLPBlox *)blox didAttemptAuthorizationWithStatusCode:(kSTLPBloxStatusCodes)statusCode authToken:(NSString *)authToken userId:(NSString *)userId message:(NSString *)message {
    if (statusCode == kSTLPBloxStatusCodeSuccess) {
        self.syncronex.userId = userId;
        [self checkAccess];
    }
    else {
        if ([self.delegate respondsToSelector:@selector(authenticationFailedWithStatusCode:message:)])
            [self.delegate authenticationFailedWithStatusCode:statusCode message:message];
    }
}

- (void)blox:(STLPBlox *)blox didFailCheckAccessWithError:(NSError *)error {
    if ([self.delegate respondsToSelector:@selector(didFailWithAnError:causedBy:)])
        [self.delegate didFailWithAnError:error causedBy:kSTLPaywallSDKErrorCauseBlox];
}

@end

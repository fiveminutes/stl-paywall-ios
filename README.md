# STL Paywall SDK & Demo application

## SDK overview

  STL Paywall SDK is used to acquire user's subscriptions via Syncronex API,
while the 'userId' itself gets returned by BLOX API based on username/password
combination, and is used internally.

  In order to use the SDK, a class instance needs to be set up with the
appropriate dictionary values. Should the default 'init' method get called, an
assertion would get triggered. The 'initWithSyncronexDictionary:bloxDictionary:'
method accepts two dictionaries with the following keys:


    Syncronex configuration:
        kSTLPSyncronexCompany
        kSTLPSyncronexProperty
        kSTLPSyncronexURLBase

    BLOX configuration:
        kSTLPBloxWSKey
        kSTLPBloxWSSecret
        kSTLPBloxURLBase

  After successful initialization, we can use 'checkAccess' method to query
Syncronex API for valid user subscriptions. As a result we could get the
following delegate methods called:


      didRequestUsernameAndPassword:

  This method gets called when ever Syncronex determines that there is no valid
'userId' provided by BLOX (e.g. this is our very first 'checkAccess' call and
user credentials have not been provided yet, login was canceled, etc.).


      didFinishAuthorizationWithResult:

  This method notifies us that, with the 'userId' provided by BLOX API, we
received some subscription data (result being 'YES'), or there were no
subscriptions returned (result being 'NO' in this case). The BLOX API gets
called internally if a valid 'userID' is found.


      didFailWithAnError:causedBy:

  This method tells us there was an error while contacting Syncronex or BLOX
servers (the culprit gets passed by '(kSTLPaywallSDKErrorCauses)causedBy'
parameter).

  Assuming this was our first access check request, we got presented with a
request for user credentials via 'didRequestUsernameAndPassword:' method. In
order to authenticate against BLOX servers, we should present a valid
username/password combination to the SDK by calling a method named
'authenticateWithUsername:password:'. This results in an internal call to BLOX
API requesting a valid 'userId' parameter, which we would in turn use to fetch
user subscriptions from Syncronex via previously mentioned 'checkAccess'. The
resulting delegate method calls could be any of the following:


      didFinishAuthorizationWithResult:
      didFailWithAnError:causedBy:

  These two have been explained previously. The one thing left to explain is
that upon a successful 'userId' retrieval, Syncronex API gets called internally
trying to fetch valid subscriptions using the newly acquired 'userId' value.


      authenticationFailedWithStatusCode:message:

  This call can be a result of an invalid username/password combination, missing
username or password or an invalid BLOX key/secret values.

  We should have an authentication result by now, with it being either 'YES' or
'NO'. Should we call 'checkAccess' again, the existing 'userId' would be used
thus bypassing BLOX authentication as it would no longer be needed.

  Should we want to force a fresh BLOX authentication, we would need to call
'clearPersistentData' prior to 'checkAccess' which nullifies 'userId'
parameter's value. That method call has no delegate calls associated with it.

